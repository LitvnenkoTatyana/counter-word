package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"sync"
)

type Result struct {
	path  string
	count int
	err   error
}

type typePath int

const (
	K          = 5
	searchWord = "Go"
	URLType    = 0
	fileType   = 1
)

var (
	ErrGetResponseHTTP = errors.New("get bad response http")
	ErrReadFile        = errors.New("error read file")
	ErrEmptyPath       = errors.New("enter path file or URL http")
	ErrBadHTTPURL      = errors.New("cant found page by this url")
)

func typeString(path string) typePath {
	parsedUrl, err := url.ParseRequestURI(path)
	if err != nil || parsedUrl.Host == "" {
		return fileType
	}
	return URLType
}

func getTextByPath(path string) (text io.ReadCloser, err error) {
	switch typeString(path) {
	case fileType:
		text, err = os.Open(path)
		if err != nil {
			return nil, ErrReadFile
		}
	case URLType:
		var resp *http.Response
		resp, err = http.Get(path)
		if err != nil {
			return nil, ErrBadHTTPURL

		}

		if resp.StatusCode != http.StatusOK {
			return nil, fmt.Errorf("%v: %v", ErrGetResponseHTTP, resp.Status)
		}
		text = resp.Body
	}
	return
}

func countWordByText(text *bufio.Reader) (count int, err error) {
	lenSearchWord := len(searchWord)
	buf := make([]byte, lenSearchWord)
	if _, err := text.Read(buf); err != nil {
		return 0, err
	}

	for {
		if bytes.Equal(buf, []byte(searchWord)) {
			count++
		}

		nextSymbol, err := text.ReadByte()
		if err != nil {
			break
		}
		buf = append(buf[1:], nextSymbol)
	}

	return
}

func countWord(path string) (res Result) {
	res.path = path
	if path == "" {
		res.err = ErrEmptyPath
		return
	}

	rowtext, err := getTextByPath(path)
	if err != nil {
		res.err = err
		return
	}
	defer rowtext.Close()

	text := bufio.NewReader(rowtext)
	res.count, res.err = countWordByText(text)

	return res
}

func main() {
	output := make(chan Result)

	scanner := bufio.NewScanner(os.Stdin)
	var wg sync.WaitGroup
	input := make(chan string)

	wg.Add(K)
	for i := 0; i < K; i++ {
		go func(input <-chan string, output chan<- Result) {
			defer wg.Done()
			for result := range input {
				output <- countWord(result)
			}
		}(input, output)
	}

	go func() {
		wg.Wait()
		close(output)
	}()

	for scanner.Scan() {
		path := scanner.Text()
		input <- path
	}
	close(input)

	var total int
	for result := range output {
		if result.err != nil {
			fmt.Printf("Error %v for: %s\n", result.err, result.path)
			continue
		}
		fmt.Printf("Count for %s: %d\n", result.path, result.count)
		total += result.count
	}
	fmt.Printf("Total: %d\n", total)
}

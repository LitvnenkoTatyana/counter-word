package main

import (
	"errors"
	"reflect"
	"testing"
)

type TestCase struct {
	incomingPath string
	result       Result
}

func TestCountWord(t *testing.T) {
	testCases := []TestCase{
		{
			incomingPath: "",
			result:       Result{path: "", err: ErrEmptyPath, count: 0},
		},
		{
			incomingPath: "/etc/passwd",
			result:       Result{path: "/etc/passwd", err: nil, count: 0},
		},
		{
			incomingPath: "https://golang.org",
			result:       Result{path: "https://golang.org", err: nil, count: 246},
		},
		{
			incomingPath: "/c/passwd",
			result:       Result{path: "/c/passwd", err: ErrReadFile, count: 0},
		},
		{
			incomingPath: "https://d.ru.problythisbadurl",
			result:       Result{path: "https://d.ru.problythisbadurl", err: ErrBadHTTPURL, count: 0},
		},
		{
			incomingPath: "https://perm.dom.ru/",
			result:       Result{path: "https://perm.dom.ru/", err: errors.New("get bad response http: 403 Forbidden"), count: 0},
		},
	}

	for i, test := range testCases {
		res := countWord(test.incomingPath)
		if !reflect.DeepEqual(res, test.result) {
			t.Errorf("test case %d unexpected err\n expected %v\n result %v", i, res, test.result)
		}
	}
}

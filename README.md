# Counter-word
help count word (case-accurate) in file or web page, enter path to file or full url
example:
 go build -o bin/go-counter ./api/
 echo -e 'https://golang.org\n/etc/passwd\nhttps://golang.org\nhttps://golang.org' | ./bin/go-counter 